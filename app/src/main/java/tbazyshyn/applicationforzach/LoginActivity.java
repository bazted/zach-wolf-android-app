package tbazyshyn.applicationforzach;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {


    // UI references.
    private TextView mResponseContent;
    private EditText mEmailView;
    private EditText mPasswordView;

    public static final String POST_BODY = "" +
            "<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
            "<s:Body>\n" +
            "<Login xmlns=\"http://mycompany.com/LoginService\">\n" +
            "<username>%s</username>\n" +
            "<password>%s</password>\n" +
            "</Login>\n" +
            "</s:Body>\n" +
            "</s:Envelope>";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (EditText) findViewById(R.id.email);
        mResponseContent = (TextView) findViewById(R.id.responseContent);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });


        attemptLogin();
    }

    public interface WCFService {


        @GET("AuthenticationSvcWrap.svc?wsdl")
        Call<ResponseBody> login(@Query("username") String login,
                                 @Query("password") String password);

    }

    private void attemptLogin() {

        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.sensor.geeklab.se/LoginWCF/")
                .client(okHttpClient)
                .build();


        WCFService wcfService = retrofit.create(WCFService.class);

        Call<ResponseBody> login = wcfService.login("Admin", "Admin34");
        login.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mResponseContent.append("===========GET REQUEST START=============\n");

                        Log.d("GET RESPONSE", "success");
                        ResponseBody body = response.body();

                        if (body != null) {
                            try {
                                mResponseContent.append(body.string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        mResponseContent.append("===========GET REQUEST END=============\n");
                    }
                });


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("GET RESPONSE ERROR", "Failure", t);
            }
        });


        Request postRequest = new Request.Builder()
                .url("http://www.sensor.geeklab.se/LoginWCF/AuthenticationSvcWrap.svc?wsdl")
                .post(
                        RequestBody.create(
                                MediaType.parse("text/xml; charset=utf-8"),
                                String.format(POST_BODY, "Admin", "Admin123")
                        )
                )
                .build();

        okHttpClient.newCall(postRequest).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                Log.e("POST RESPONSE ERROR", "Failure", e);

            }

            @Override
            public void onResponse(okhttp3.Call call, final okhttp3.Response response) throws IOException {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mResponseContent.append("===========POST REQUEST START=============\n");

                        Log.d("POST RESPONSE", "success");
                        ResponseBody body = response.body();

                        if (body != null) {
                            try {
                                mResponseContent.append(body.string());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        mResponseContent.append("===========POST REQUEST END=============\n");


                    }
                });

            }
        });

    }


}

